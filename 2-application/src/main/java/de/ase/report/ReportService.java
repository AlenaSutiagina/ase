package de.ase.report;

import de.ase.month.Month;
import de.ase.month.MonthService;

import java.io.IOException;
import java.util.Optional;

public class ReportService {

	private MonthService monthService;

	public ReportService(MonthService monthService) {
		this.monthService = monthService;
	}

	public Optional<Report> createReport(String firstMonthName, int firstYearNum, String secondMonthName, int secondYearNum) throws IOException {
		Optional<Month> firstMonth = monthService.getMonth(firstMonthName, firstYearNum);
		Optional<Month> secondMonth = monthService.getMonth(secondMonthName, secondYearNum);
		if (firstMonth.isEmpty() || secondMonth.isEmpty()) {
			return Optional.empty();
		}
		return Optional.of(new Report(firstMonth.get(), secondMonth.get()));
	}
}
