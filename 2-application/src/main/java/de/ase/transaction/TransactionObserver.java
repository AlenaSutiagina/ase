package de.ase.transaction;

import java.util.List;

public interface TransactionObserver {

	void updateTransactions(List<Transaction> transactions);

}
