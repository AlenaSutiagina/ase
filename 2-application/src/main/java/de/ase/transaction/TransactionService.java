package de.ase.transaction;

import de.ase.month.MonthService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class TransactionService implements TransactionSubject {

	private List<TransactionObserver> observers = new ArrayList<>();

	private final TransactionRepository transactionRepository;
	private final MonthService monthService;

	public TransactionService(TransactionRepository transactionRepository, MonthService monthService) {
		this.transactionRepository = transactionRepository;
		this.monthService = monthService;
	}

	public List<Transaction> getTransactionsForCurrentMonth(UUID id) throws IOException {
		if (id == null) {
			throw new IllegalArgumentException("Month is null");
		}
		List<Transaction> transactionsForCurrentMonth = new ArrayList<>();
		List<Transaction> allTransactions = transactionRepository.getAll();
		for (Transaction transaction : allTransactions) {
			if (transaction.getMonth().equals(id)) {
				transactionsForCurrentMonth.add(transaction);
			}
		}
		return transactionsForCurrentMonth;
	}

	public void saveTransaction(Transaction transaction) throws IOException {
		if (transaction == null) {
			throw new IllegalArgumentException("Transaction can not be null");
		}
		if (transaction.getAmount() < 0) {
			throw new IllegalArgumentException("Transaction amount can not be negative");
		}
		this.monthService.updateBalance(transaction.isIncome(), false, transaction.getAmount());
		this.transactionRepository.save(transaction);
		notifyObservers(getTransactionsForCurrentMonth(monthService.getCurrentMonth().getId()));
	}

	public void deleteTransaction(Transaction transaction) throws IOException {
		if (transaction == null) {
			throw new IllegalArgumentException("Transaction can not be null");
		}
		this.monthService.updateBalance(transaction.isIncome(), true, transaction.getAmount());
		this.transactionRepository.delete(transaction);
		notifyObservers(getTransactionsForCurrentMonth(monthService.getCurrentMonth().getId()));
	}

	@Override
	public void registerObserver(TransactionObserver observer) {
		observers.add(observer);
	}

	@Override
	public void unregisterObserver(TransactionObserver observer) {
		observers.remove(observer);
	}

	@Override
	public void notifyObservers(List<Transaction> transactions) {
		for (TransactionObserver observer : observers) {
			observer.updateTransactions(transactions);
		}
	}
}
