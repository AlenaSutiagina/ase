package de.ase.transaction;

import java.util.List;

public interface TransactionSubject {

	void registerObserver(TransactionObserver observer);

	void unregisterObserver(TransactionObserver observer);

	void notifyObservers(List<Transaction> transactions);

}
