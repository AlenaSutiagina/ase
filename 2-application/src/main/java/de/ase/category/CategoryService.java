package de.ase.category;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CategoryService {
	private final CategoryRepository categoryRepository;

	public CategoryService(CategoryRepository categoryRepository) {
		this.categoryRepository = categoryRepository;
	}

	public List<Category> getAllCategories() throws IOException {
		return categoryRepository.findAll();
	}

	public List<Category> getCategoriesOfOneType(boolean type) throws IOException {
		List<Category> categoriesOfOneType = new ArrayList<>();
		List<Category> categories = categoryRepository.findAll();
		categories.forEach(category -> {
			if (category.isIncome() == type) {
				categoriesOfOneType.add(category);
			}
		});
		return categoriesOfOneType;
	}

	public void saveCategory(Category category) throws IOException {
		if (category == null || category.getName() == null) {
			throw new IllegalArgumentException("Category is null");
		}
		if (categoryRepository.getByName(category.getName()).isPresent()) {
			throw new IllegalArgumentException("Category name '" + category.getName() + "' is already used");
		}
		categoryRepository.save(category);
	}
}
