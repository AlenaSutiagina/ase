package de.ase.month;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class MonthService {

	private final MonthRepository monthRepository;

	public MonthService(MonthRepository monthRepository) {
		this.monthRepository = monthRepository;
	}

	public Month getCurrentMonth() throws IOException {
		LocalDate currentDate = LocalDate.now();
		List<Month> allMonths = monthRepository.getAll();
		for (int i = 0; i < allMonths.size(); i++) {
			if (allMonths.get(i).getName().equalsIgnoreCase(String.valueOf(currentDate.getMonth())) && allMonths.get(i).getYear() == currentDate.getYear()) {
				return allMonths.get(i);
			}
		}
		Month month = new Month(currentDate.getMonth().toString(), currentDate.getYear(), allMonths.size() == 0 ? 0 : allMonths.get(allMonths.size() - 1).getActualBalance(), allMonths.size() == 0 ? 0 : allMonths.get(allMonths.size() - 1).getActualBalance(), 0, 0);
		monthRepository.save(month);
		return month;
	}

	public ArrayList<String> getAllMonthNames() throws IOException {
		ArrayList<String> monthNames = new ArrayList<>();
		List<Month> allMonths = monthRepository.getAll();
		for (Month month : allMonths) {
			String name = month.getName();
			if (!monthNames.contains(name)) {
				monthNames.add(name);
			}
		}
		return monthNames;
	}

	public ArrayList<Integer> getAllYears() throws IOException {
		ArrayList<Integer> monthYears = new ArrayList<>();
		List<Month> allMonths = monthRepository.getAll();
		for (Month month : allMonths) {
			int year = month.getYear();
			if (!monthYears.contains(year)) {
				monthYears.add(year);
			}
		}
		return monthYears;
	}

	public Optional<Month> getMonth(String name, int year) throws IOException {
		if (name == null) {
			throw new IllegalArgumentException("Month name is empty");
		}
		List<Month> allMonths = monthRepository.getAll();
		for (int i = 0; i < allMonths.size(); i++) {
			if (allMonths.get(i).getName().equalsIgnoreCase(name) && allMonths.get(i).getYear() == year) {
				return Optional.of(allMonths.get(i));
			}
		}
		return Optional.empty();
	}

	public void updateBalance(boolean income, boolean deleteTransaction, double amount) throws IOException {
		Month currentMonth = getCurrentMonth();
		currentMonth.setActualBalance(currentMonth.getActualBalance() + balanceChange(income, deleteTransaction, amount));
		currentMonth.setSumIncomes(currentMonth.getSumIncomes() + incomeChange(income, deleteTransaction, amount));
		currentMonth.setSumPayouts(currentMonth.getSumPayouts() + payoutChange(income, deleteTransaction, amount));
		monthRepository.updateBalance(currentMonth);
	}

	private double balanceChange(boolean income, boolean deleteTransaction, double amount) {
		double balanceChange;
		if (deleteTransaction) {
			balanceChange = income ? -amount : amount;
		} else {
			balanceChange = income ? amount : -amount;
		}
		return balanceChange;
	}

	private double incomeChange(boolean income, boolean deleteTransaction, double amount) {
		double incomeChange;
		if (deleteTransaction) {
			incomeChange = income ? -amount : 0;
		} else {
			incomeChange = income ? amount : 0;
		}
		return incomeChange;
	}

	private double payoutChange(boolean income, boolean deleteTransaction, double amount) {
		double payoutChange;
		if (deleteTransaction) {
			payoutChange = income ? 0 : -amount;
		} else {
			payoutChange = income ? 0 : amount;
		}
		return payoutChange;
	}

}
