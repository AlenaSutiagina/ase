package de.ase.report;

import de.ase.month.Month;
import de.ase.month.MonthService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.io.IOException;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

public class ReportServiceTest {
	@Mock
	private MonthService monthService;
	private ReportService reportService;

	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);
		reportService = new ReportService(monthService);
	}

	@Test
	public void testCreateReportWithValidMonths() throws IOException {
		Month firstMonth = mock(Month.class);
		Month secondMonth = mock(Month.class);

		when(monthService.getMonth("January", 2023)).thenReturn(Optional.of(firstMonth));
		when(monthService.getMonth("February", 2023)).thenReturn(Optional.of(secondMonth));

		Optional<Report> report = reportService.createReport("January", 2023, "February", 2023);

		assertTrue(report.isPresent());
		assertEquals(firstMonth, report.get().getFirstMonth());
		assertEquals(secondMonth, report.get().getSecondMonth());

		Mockito.verify(monthService, Mockito.times(1)).getMonth("January", 2023);
		Mockito.verify(monthService, Mockito.times(1)).getMonth("February", 2023);
		verifyNoMoreInteractions(monthService);
	}

	@Test
	public void testCreateReportWithInvalidMonths() throws IOException {
		when(monthService.getMonth("January", 2023)).thenReturn(Optional.empty());

		Optional<Report> report = reportService.createReport("January", 2023, "February", 2023);

		assertFalse(report.isPresent());
	}
}