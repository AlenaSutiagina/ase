package de.ase.category;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.awt.*;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

public class CategoryServiceTest {

	@Mock
	private CategoryRepository categoryRepository;
	private CategoryService categoryService;

	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);
		categoryService = new CategoryService(categoryRepository);
	}

	@Test
	public void testGetAllCategories() throws IOException {
		Category category1 = new Category(true, "Category1", new Color(12, 12, 12));
		Category category2 = new Category(false, "Category2", new Color(33, 12, 12));

		when(categoryRepository.findAll()).thenReturn(List.of(category1, category2));

		List<Category> allCategories = categoryService.getAllCategories();

		assertEquals(List.of(category1, category2), allCategories);

		Mockito.verify(categoryRepository, Mockito.times(1)).findAll();
		verifyNoMoreInteractions(categoryRepository);
	}

	@Test
	public void testGetCategoriesOfOneTypeWithIncome() throws IOException {

		Category category1 = new Category(true, "Category1", new Color(12, 12, 12));
		Category category2 = new Category(false, "Category2", new Color(33, 12, 12));
		Category category3 = new Category(true, "Category3", new Color(34, 12, 12));

		when(categoryRepository.findAll()).thenReturn(List.of(category1, category2, category3));

		List<Category> incomeCategories = categoryService.getCategoriesOfOneType(true);

		assertEquals(List.of(category1, category3), incomeCategories);

		Mockito.verify(categoryRepository, Mockito.times(1)).findAll();
		verifyNoMoreInteractions(categoryRepository);
	}

	@Test
	public void testGetCategoriesOfOneTypeWithExpense() throws IOException {
		Category category1 = new Category(true, "Category1", new Color(12, 12, 12));
		Category category2 = new Category(false, "Category2", new Color(33, 12, 12));
		Category category3 = new Category(true, "Category3", new Color(34, 12, 12));

		when(categoryRepository.findAll()).thenReturn(List.of(category1, category2, category3));

		List<Category> expenseCategories = categoryService.getCategoriesOfOneType(false);

		assertEquals(List.of(category2), expenseCategories);

		Mockito.verify(categoryRepository, Mockito.times(1)).findAll();
		verifyNoMoreInteractions(categoryRepository);
	}

	@Test
	public void testSaveCategory() throws IOException {
		Category category = new Category(true, "Category1", new Color(12, 12, 12));

		when(categoryRepository.getByName(category.getName())).thenReturn(Optional.empty());

		categoryService.saveCategory(category);

		Mockito.verify(categoryRepository, Mockito.times(1)).getByName(category.getName());
		Mockito.verify(categoryRepository, Mockito.times(1)).save(category);
		verifyNoMoreInteractions(categoryRepository);
	}

	@Test
	public void testSaveCategoryWithExistingCategoryName() throws IOException {
		Category category = new Category(true, "Category1", new Color(12, 12, 12));

		when(categoryRepository.getByName(category.getName())).thenReturn(Optional.of(category));
		assertThrows(IllegalArgumentException.class, () -> categoryService.saveCategory(category));

		Mockito.verify(categoryRepository, Mockito.times(1)).getByName(category.getName());
		verifyNoMoreInteractions(categoryRepository);
	}

	@Test
	public void testSaveNullCategory() {
		assertThrows(IllegalArgumentException.class, () -> categoryService.saveCategory(null));

		verifyNoMoreInteractions(categoryRepository);
	}

}