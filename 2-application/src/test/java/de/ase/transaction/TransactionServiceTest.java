package de.ase.transaction;

import de.ase.category.Category;
import de.ase.month.Month;
import de.ase.month.MonthService;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verifyNoMoreInteractions;

public class TransactionServiceTest {

	@Mock
	private TransactionRepository transactionRepository;

	@Mock
	private MonthService monthService;

	private TransactionService transactionService;

	Category incomeCategory = new Category(true, "income", new Color(12, 12, 12));
	Category paymentCategory = new Category(false, "payment", new Color(33, 12, 12));

	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);
		transactionService = new TransactionService(transactionRepository, monthService);
	}

	@Test
	public void testGetTransactionsForCurrentMonth() throws IOException {
		UUID monthId = UUID.randomUUID();
		List<Transaction> allTransactions = new ArrayList<>();
		Transaction transaction1 = new Transaction(true, 100, "2023-10-10", monthId, incomeCategory);
		Transaction transaction2 = new Transaction(false, 200, "2023-10-10", UUID.randomUUID(), paymentCategory);
		Transaction transaction3 = new Transaction(true, 300, "2023-10-10", monthId, incomeCategory);
		allTransactions.add(transaction1);
		allTransactions.add(transaction2);
		allTransactions.add(transaction3);

		Mockito.when(transactionRepository.getAll()).thenReturn(allTransactions);

		List<Transaction> result = transactionService.getTransactionsForCurrentMonth(monthId);

		Assertions.assertEquals(2, result.size());
		Assertions.assertTrue(result.contains(transaction1));
		Assertions.assertTrue(result.contains(transaction3));
		Mockito.verify(transactionRepository, Mockito.times(1)).getAll();
		verifyNoMoreInteractions(transactionRepository);
	}

	@Test
	public void testSaveTransaction() throws IOException {
		Transaction transaction = new Transaction(true, 150, "2023-10-10", UUID.randomUUID(), incomeCategory);
		Month month = new Month("April", 2023, 10, 10, 0, 0);

		Mockito.when(monthService.getCurrentMonth()).thenReturn(month);

		transactionService.saveTransaction(transaction);

		Mockito.verify(monthService, Mockito.times(1)).updateBalance(true, false, 150);
		Mockito.verify(transactionRepository, Mockito.times(1)).save(transaction);
	}

	@Test
	public void testSaveTransactionThrowsExceptionIfNegativeAmount() throws IOException {
		Transaction transaction = new Transaction(true, -150, "2023-10-10", UUID.randomUUID(), incomeCategory);

		assertThrows(IllegalArgumentException.class, () -> transactionService.saveTransaction(transaction));
		Mockito.verify(transactionRepository, Mockito.times(0)).save(transaction);
		verifyNoMoreInteractions(transactionRepository);
	}

	@Test
	public void testDeleteTransaction() throws IOException {
		Transaction transaction = new Transaction(false, 200, "2023-10-10", UUID.randomUUID(), paymentCategory);
		Month month = new Month("April", 2023, 10, 10, 0, 0);

		Mockito.when(monthService.getCurrentMonth()).thenReturn(month);
		transactionService.deleteTransaction(transaction);

		Mockito.verify(monthService, Mockito.times(1)).updateBalance(false, true, 200);
		Mockito.verify(transactionRepository, Mockito.times(1)).delete(transaction);
	}

	@Test
	public void testThrowsExceptionIfMonthIdIsNull() {
		assertThrows(IllegalArgumentException.class, () -> transactionService.getTransactionsForCurrentMonth(null));
		verifyNoMoreInteractions(transactionRepository);
	}

	@Test
	public void testSaveThrowsExceptionIfTraansactionIsNull() {
		assertThrows(IllegalArgumentException.class, () -> transactionService.saveTransaction(null));
		verifyNoMoreInteractions(transactionRepository);
	}

	@Test
	public void testDeleteThrowsExceptionIfTraansactionIsNull() {
		assertThrows(IllegalArgumentException.class, () -> transactionService.deleteTransaction(null));
		verifyNoMoreInteractions(transactionRepository);
	}
}