package de.ase.month;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

public class MonthServiceTest {

	@Mock
	private MonthRepository monthRepository;
	private MonthService monthService;

	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);
		monthService = new MonthService(monthRepository);
	}

	@Test
	public void testGetCurrentMonth() throws IOException {
		Month existingMonth = new Month("May", 2023, 100.0, 100.0, 0, 0);

		when(monthRepository.getAll()).thenReturn(List.of(existingMonth));

		Month currentMonth = monthService.getCurrentMonth();

		assertEquals(existingMonth, currentMonth);

		Mockito.verify(monthRepository, Mockito.times(1)).getAll();
	}

	@Test
	public void testGetAllMonthNames() throws IOException {
		Month month1 = new Month("January", 2023, 0, 0, 0, 0);
		Month month2 = new Month("February", 2023, 0, 0, 0, 0);

		when(monthRepository.getAll()).thenReturn(List.of(month1, month2));

		ArrayList<String> monthNames = monthService.getAllMonthNames();

		assertEquals(List.of("January", "February"), monthNames);

		Mockito.verify(monthRepository, Mockito.times(1)).getAll();
		verifyNoMoreInteractions(monthRepository);
	}

	@Test
	public void testGetAllYears() throws IOException {
		Month month1 = new Month("January", 2023, 0, 0, 0, 0);
		Month month2 = new Month("February", 2023, 0, 0, 0, 0);

		when(monthRepository.getAll()).thenReturn(List.of(month1, month2));

		ArrayList<Integer> monthYears = monthService.getAllYears();

		assertEquals(List.of(2023), monthYears);

		Mockito.verify(monthRepository, Mockito.times(1)).getAll();
	}

	@Test
	public void testGetMonthWithExistingMonth() throws IOException {
		Month existingMonth = new Month("January", 2023, 0, 0, 0, 0);

		when(monthRepository.getAll()).thenReturn(List.of(existingMonth));

		Optional<Month> month = monthService.getMonth("January", 2023);

		assertTrue(month.isPresent());
		assertEquals(existingMonth, month.get());

		Mockito.verify(monthRepository, Mockito.times(1)).getAll();
		verifyNoMoreInteractions(monthRepository);
	}

	@Test
	public void testGetMonthWithNonExistingMonth() throws IOException {
		Month existingMonth = new Month("January", 2023, 0, 0, 0, 0);

		when(monthRepository.getAll()).thenReturn(List.of(existingMonth));

		Optional<Month> month = monthService.getMonth("February", 2023);

		assertFalse(month.isPresent());

		Mockito.verify(monthRepository, Mockito.times(1)).getAll();
		verifyNoMoreInteractions(monthRepository);
	}

	@Test
	public void testUpdateBalance() throws IOException {
		Month currentMonth = new Month("May", 2023, 100.0, 100.0, 0, 0);

		when(monthRepository.getAll()).thenReturn(List.of(currentMonth));

		monthService.updateBalance(true, false, 50.0);

		assertEquals(150.0, currentMonth.getActualBalance(), 0.0);
		assertEquals(50.0, currentMonth.getSumIncomes(), 0.0);

		Mockito.verify(monthRepository, Mockito.times(1)).getAll();
		Mockito.verify(monthRepository, Mockito.times(1)).updateBalance(currentMonth);
		verifyNoMoreInteractions(monthRepository);
	}

	@Test
	public void testExceptionThrownIfMonthNameIsNull() {
		assertThrows(IllegalArgumentException.class, () -> monthService.getMonth(null, 2012));
		verifyNoMoreInteractions(monthRepository);
	}
}