package de.ase.report;

import de.ase.month.Month;

public class Report {

	private final Month firstMonth;
	private final Month secondMonth;

	public Report(Month firstMonth, Month secondMonth) {
		this.firstMonth = firstMonth;
		this.secondMonth = secondMonth;
	}

	public Month getFirstMonth() {
		return firstMonth;
	}

	public Month getSecondMonth() {
		return secondMonth;
	}

}
