package de.ase.month;

import java.io.IOException;
import java.util.List;

public interface MonthRepository {

	List<Month> getAll() throws IOException;

	void save(Month month) throws IOException;

	void updateBalance(Month month) throws IOException;
}
