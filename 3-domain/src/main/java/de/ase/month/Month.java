package de.ase.month;

import java.util.Objects;
import java.util.UUID;

public class Month {

	private UUID id;
	private String name;
	private int year;
	private double beginningBalance;
	private double actualBalance;
	private double sumPayouts;
	private double sumIncomes;

	public Month() {
	}

	public Month(String name, int year, double beginningBalance, double actualBalance, double sumPayouts, double sumIncomes) {
		this.id = UUID.randomUUID();
		this.name = name;
		this.year = year;
		this.beginningBalance = beginningBalance;
		this.actualBalance = actualBalance;
		this.sumPayouts = sumPayouts;
		this.sumIncomes = sumIncomes;
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public double getBeginningBalance() {
		return beginningBalance;
	}

	public void setBeginningBalance(double beginningBalance) {
		this.beginningBalance = beginningBalance;
	}

	public double getActualBalance() {
		return actualBalance;
	}

	public void setActualBalance(double actualBalance) {
		this.actualBalance = actualBalance;
	}

	public double getSumPayouts() {
		return sumPayouts;
	}

	public void setSumPayouts(double sumPayouts) {
		this.sumPayouts = sumPayouts;
	}

	public double getSumIncomes() {
		return sumIncomes;
	}

	public void setSumIncomes(double sumIncomes) {
		this.sumIncomes = sumIncomes;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Month month = (Month) o;
		return Objects.equals(id, month.id);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, name, year, beginningBalance, actualBalance, sumPayouts, sumIncomes);
	}
}
