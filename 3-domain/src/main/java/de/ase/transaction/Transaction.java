package de.ase.transaction;


import de.ase.category.Category;

import java.util.Date;
import java.util.Objects;
import java.util.UUID;

public class Transaction {

	private UUID id;

	private boolean income;
	private double amount;
	private String date;
	private UUID month;
	private Category category;
	public Transaction() {
	}

	public Transaction(boolean income, double amount, String date, UUID month, Category category) {
		this.id = UUID.randomUUID();
		this.income = income;
		this.amount = amount;
		this.date = date;
		this.month = month;
		this.category = category;
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public boolean isIncome() {
		return income;
	}

	public void setIncome(boolean income) {
		this.income = income;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public UUID getMonth() {
		return month;
	}

	public void setMonth(UUID month) {
		this.month = month;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Transaction that = (Transaction) o;
		return Objects.equals(id, that.id);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, income, amount, date, month, category);
	}
}
