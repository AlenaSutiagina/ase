package de.ase.transaction;

import java.io.IOException;
import java.util.List;

public interface TransactionRepository {

	List<Transaction> getAll() throws IOException;

	void save(Transaction transaction) throws IOException;

	void delete(Transaction transaction) throws IOException;
}
