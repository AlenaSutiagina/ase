package de.ase.category;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import de.ase.color.ColorDeserializer;
import de.ase.color.ColorSerializer;

import java.awt.*;
import java.util.Objects;
import java.util.UUID;

public class Category {

	private UUID id;

	private boolean income;
	private String name;

	@JsonDeserialize(using = ColorDeserializer.class)
	@JsonSerialize(using = ColorSerializer.class)
	private Color color;

	public Category() {
	}

	public Category(boolean income, String name, Color color) {
		this.id = UUID.randomUUID();
		this.income = income;
		this.name = name;
		this.color = color;
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public void setIncome(boolean income) {
		this.income = income;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public boolean isIncome() {
		return income;
	}

	public String getName() {
		return name;
	}

	public Color getColor() {
		return color;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Category category = (Category) o;
		return Objects.equals(id, category.id);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, income, name, color);
	}
}
