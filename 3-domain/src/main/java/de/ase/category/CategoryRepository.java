package de.ase.category;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

public interface CategoryRepository {

	List<Category> findAll() throws IOException;

	Optional<Category> getByName(String name) throws IOException;

	void save(Category category) throws IOException;
}
