package de.ase.color;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.awt.*;
import java.io.IOException;

public class ColorSerializer extends JsonSerializer<Color> {
	@Override
	public void serialize(Color color, JsonGenerator jgen, SerializerProvider provider) throws IOException {
		jgen.writeStartObject();
		jgen.writeNumberField("r", color.getRed());
		jgen.writeNumberField("g", color.getGreen());
		jgen.writeNumberField("b", color.getBlue());
		jgen.writeEndObject();
	}
}
