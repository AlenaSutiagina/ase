package de.ase.gui.report;

import com.formdev.flatlaf.FlatLightLaf;
import de.ase.category.Category;
import de.ase.category.CategoryService;
import de.ase.report.Report;
import de.ase.report.ReportService;
import de.ase.transaction.Transaction;
import de.ase.transaction.TransactionService;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

public class GUIReport extends JFrame {

	public GUIReport(ReportService reportService, CategoryService categoryService, TransactionService transactionService, String firstMonthName, int firstYearNum, String secondMonthName, int secondYearNum) throws UnsupportedLookAndFeelException, IOException {
		UIManager.setLookAndFeel(new FlatLightLaf());

		JFrame frame = new JFrame("Report " + firstMonthName + " " + firstYearNum + " - " + secondMonthName + " " + secondYearNum);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

		List<Category> categories = categoryService.getAllCategories();
		Optional<Report> report = reportService.createReport(firstMonthName, firstYearNum, secondMonthName, secondYearNum);
		if (report.isEmpty()) {
			JOptionPane.showMessageDialog(frame, "No report was created", "Error", JOptionPane.ERROR_MESSAGE);
			frame.dispose();
		}

		JSplitPane northSouthSplit = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
		JSplitPane southWestEastSplit = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);

		List<Transaction> transactionsFirstMonth = transactionService.getTransactionsForCurrentMonth(report.get().getFirstMonth().getId());
		List<Transaction> transactionsSecondMonth = transactionService.getTransactionsForCurrentMonth(report.get().getSecondMonth().getId());

		JPanel leftPanel = new JPanel(new BorderLayout());
		JPanel rightPanel = new JPanel(new GridLayout(2, 1));

		JTable table = paintTable(categories, report, firstMonthName, firstYearNum, secondMonthName, secondYearNum, transactionsFirstMonth, transactionsSecondMonth);
		northSouthSplit.setBottomComponent(new JScrollPane(table));

		southWestEastSplit.setLeftComponent(leftPanel);
		southWestEastSplit.setRightComponent(rightPanel);
		northSouthSplit.setTopComponent(southWestEastSplit);
		frame.add(northSouthSplit);
		frame.pack();
		frame.setVisible(true);

	}

	private double getAmountForEachCategory(Category category, List<Transaction> transactions) {
		double amount = 0;
		for (Transaction value : transactions) {
			if (value.getCategory().equals(category)) {
				amount += value.getAmount();
			}
		}
		return amount;
	}

	private JTable paintTable(List<Category> categories, Optional<Report> report, String firstMonthName, int firstYearNum, String secondMonthName, int secondYearNum, List<Transaction> transactionsFirstMonth, List<Transaction> transactionsSecondMonth) {
		String[] columnNames = {"Category", firstMonthName + " " + firstYearNum, secondMonthName + " " + secondYearNum, "Difference"};
		Object[][] data = new Object[categories.size() + 2][4];
		data[0][0] = "Total Incomes";
		data[0][1] = report.get().getFirstMonth().getSumIncomes();
		data[0][2] = report.get().getSecondMonth().getSumIncomes();
		data[0][3] = report.get().getFirstMonth().getSumIncomes() - report.get().getSecondMonth().getSumIncomes();
		data[1][0] = "Total Transactions";
		data[1][1] = report.get().getFirstMonth().getSumPayouts();
		data[1][2] = report.get().getSecondMonth().getSumPayouts();
		data[1][3] = report.get().getFirstMonth().getSumPayouts() - report.get().getSecondMonth().getSumPayouts();
		;
		for (int i = 0; i < categories.size(); i++) {
			data[i + 2][0] = categories.get(i).getName();
			double amountFirst = getAmountForEachCategory(categories.get(i), transactionsFirstMonth);
			double amountSecond = getAmountForEachCategory(categories.get(i), transactionsSecondMonth);
			data[i + 2][1] = amountFirst;
			data[i + 2][2] = amountSecond;
			data[i + 2][3] = amountFirst - amountSecond;
		}
		DefaultTableModel model = new DefaultTableModel(data, columnNames);
		JTable table = new JTable(model);
		return table;
	}
}
