package de.ase.gui.transaction;

import com.formdev.flatlaf.FlatLightLaf;
import de.ase.month.MonthService;
import de.ase.transaction.Transaction;
import de.ase.transaction.TransactionObserver;
import de.ase.transaction.TransactionService;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.util.List;

public class GUIAllTransactions extends JDialog implements TransactionObserver {

	private TransactionService transactionService;
	private MonthService monthService;
	private JSplitPane southWestEastSplit = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
	private JDialog frame;


	public GUIAllTransactions(TransactionService transactionService,
	                          MonthService monthService) throws UnsupportedLookAndFeelException, IOException {
		frame = new JDialog();
		this.transactionService = transactionService;
		this.monthService = monthService;
		this.transactionService.registerObserver(this);
		UIManager.setLookAndFeel(new FlatLightLaf());

		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

		paintUI();

		frame.add(southWestEastSplit);
		frame.pack();
		frame.setVisible(true);
	}

	private void paintUI() throws IOException {
		List<Transaction> transactions = transactionService.getTransactionsForCurrentMonth(monthService.getCurrentMonth().getId());
		JPanel infoPanel = new JPanel();
		infoPanel.setLayout(new GridLayout(transactions.size() + 1, 5));
		infoPanel.add(new JLabel("Date"));
		infoPanel.add(new JLabel("Category"));
		infoPanel.add(new JLabel("Amount"));
		infoPanel.add(new JLabel("Income"));
		infoPanel.add(new JLabel(""));

		for (Transaction tr : transactions) {
			JButton deleteTransaction = paintButtonDeleteTransaction(tr);
			infoPanel.add(new JLabel(tr.getDate()));
			infoPanel.add(new JLabel(tr.getCategory().getName()));
			infoPanel.add(new JLabel(String.valueOf(tr.getAmount())));
			infoPanel.add(new JLabel(tr.isIncome() ? "\u2713" : ""));
			infoPanel.add(deleteTransaction);
		}
		southWestEastSplit.setTopComponent(infoPanel);
		revalidate();
		repaint();
	}

	@Override
	public void updateTransactions(List<Transaction> transactions) {
		try {
			paintUI();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private JButton paintButtonDeleteTransaction(Transaction transaction) {
		JButton deleteTransaction = new JButton("Delete transaction");
		deleteTransaction.addActionListener((e -> {
			try {
				transactionService.deleteTransaction(transaction);
			} catch (IOException ex) {
				ex.printStackTrace();
				JOptionPane.showMessageDialog(frame, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			}
		}));
		return deleteTransaction;
	}
}
