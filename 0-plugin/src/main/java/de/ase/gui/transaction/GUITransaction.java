package de.ase.gui.transaction;

import com.formdev.flatlaf.FlatLightLaf;
import de.ase.category.Category;
import de.ase.category.CategoryService;
import de.ase.transaction.Transaction;
import de.ase.transaction.TransactionService;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.UUID;

public class GUITransaction extends JDialog {

	private JPanel rightPanel;
	private JTextField amountField;
	JDialog frame;

	public GUITransaction(TransactionService transactionService, CategoryService categoryService, boolean income, UUID month) throws UnsupportedLookAndFeelException, IOException {
		UIManager.setLookAndFeel(new FlatLightLaf());

		List<Category> categories = categoryService.getCategoriesOfOneType(income);

		frame = new JDialog();
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

		JSplitPane northSouthSplit = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
		JSplitPane southWestEastSplit = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);

		JPanel leftPanel = new JPanel(new BorderLayout());
		rightPanel = new JPanel(new GridLayout(categories.size(), 1));

		amountField = new JTextField();
		JButton saveButton = paintSaveButton(categories, transactionService, income, month);
		leftPanel.add(amountField, BorderLayout.CENTER);
		leftPanel.add(saveButton, BorderLayout.EAST);

		paintButtonGroup(categories);

		southWestEastSplit.setLeftComponent(leftPanel);
		southWestEastSplit.setRightComponent(rightPanel);
		northSouthSplit.setTopComponent(southWestEastSplit);
		frame.add(northSouthSplit);
		frame.pack();
		frame.setVisible(true);
	}

	private JButton paintSaveButton(List<Category> categories, TransactionService transactionService, boolean income, UUID month) {
		JButton saveButton = new JButton("Save");
		LocalDate currentDate = LocalDate.now();
		saveButton.addActionListener(e -> {
			for (int i = 0; i < categories.size(); i++) {
				JRadioButton radioButton = (JRadioButton) rightPanel.getComponent(i);
				if (radioButton.isSelected()) {
					DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
					String formattedDate = currentDate.format(formatter);
					try {
						transactionService.saveTransaction(new Transaction(income, Double.parseDouble(amountField.getText()), formattedDate, month, categories.get(i)));
					} catch (IOException | IllegalArgumentException ex) {
						ex.printStackTrace();
						JOptionPane.showMessageDialog(frame, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
					}
					break;
				}
			}
			frame.dispose();
		});
		return saveButton;
	}

	private ButtonGroup paintButtonGroup(List<Category> categories) {
		ButtonGroup buttonGroup = new ButtonGroup();
		for (Category category : categories) {
			JRadioButton radioButton = new JRadioButton(category.getName());
			radioButton.setForeground(category.getColor());
			buttonGroup.add(radioButton);
			rightPanel.add(radioButton);
		}
		return buttonGroup;
	}
}

