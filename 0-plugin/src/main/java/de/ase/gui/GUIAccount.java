package de.ase.gui;

import com.formdev.flatlaf.FlatLightLaf;
import de.ase.category.Category;
import de.ase.category.CategoryService;
import de.ase.gui.category.GUINewCategory;
import de.ase.gui.report.GUIReport;
import de.ase.gui.transaction.GUIAllTransactions;
import de.ase.gui.transaction.GUITransaction;
import de.ase.month.Month;
import de.ase.month.MonthService;
import de.ase.report.ReportService;
import de.ase.transaction.Transaction;
import de.ase.transaction.TransactionObserver;
import de.ase.transaction.TransactionService;
import org.knowm.xchart.PieChart;
import org.knowm.xchart.PieChartBuilder;
import org.knowm.xchart.XChartPanel;
import org.knowm.xchart.style.Styler;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.List;

public class GUIAccount extends JFrame implements TransactionObserver {

	private CategoryService categoryService;
	private MonthService monthService;
	private TransactionService transactionService;
	private ReportService reportService;
	private JSplitPane northSouthSplit = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
	private JSplitPane southWestEastSplit = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
	private String[] columnNames = {"Category", "Date", "Amount"};
	private JFrame frame;

	public GUIAccount(CategoryService categoryService, MonthService monthService, TransactionService transactionService, ReportService reportService) throws UnsupportedLookAndFeelException, IOException {

		this.categoryService = categoryService;
		this.monthService = monthService;
		this.transactionService = transactionService;
		this.reportService = reportService;
		this.transactionService.registerObserver(this);

		Month month = monthService.getCurrentMonth();

		UIManager.setLookAndFeel(new FlatLightLaf());

		frame = new JFrame("Financial Controller");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		paintFrame();

		southWestEastSplit.setResizeWeight(0.2);
		northSouthSplit.setTopComponent(southWestEastSplit);
		northSouthSplit.setResizeWeight(0.5);

		JPanel buttons = paintButtonsPanel(month);

		frame.add(northSouthSplit, BorderLayout.CENTER);
		frame.add(buttons, BorderLayout.SOUTH);
		frame.pack();
		frame.setVisible(true);
	}

	private void paintFrame() {
		try {
			List<Category> categories = categoryService.getAllCategories();
			Month month = monthService.getCurrentMonth();
			List<Transaction> transactions = transactionService.getTransactionsForCurrentMonth(month.getId());

			JTable table = paintTable(categories, month, transactions);
			JPanel infoPanel = paintInfoPanel(month);
			JPanel panel = paintPieChart(categories, transactions);

			northSouthSplit.setBottomComponent(new JScrollPane(table));
			southWestEastSplit.setLeftComponent(infoPanel);
			southWestEastSplit.setRightComponent(panel);

			revalidate();
			repaint();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private double getAmountForEachCategory(Category category, List<Transaction> transactions) {
		double amount = 0;
		for (Transaction value : transactions) {
			if (value.getCategory().equals(category)) {
				amount += value.getAmount();
			}
		}
		return amount;
	}

	private JPanel paintInfoPanel(Month month) {
		JPanel infoPanel = new JPanel();
		infoPanel.setLayout(new GridLayout(5, 2));

		JButton allTransactions = new JButton("get all transactions");
		allTransactions.addActionListener((e -> {
			try {
				new GUIAllTransactions(transactionService, monthService);
			} catch (UnsupportedLookAndFeelException | IOException ex) {
				ex.printStackTrace();
			}
		}));

		infoPanel.add(new JLabel("Actual balance: "));
		infoPanel.add(new JLabel(String.valueOf(month.getActualBalance())));
		infoPanel.add(new JLabel("Balance in the beginning of the month: "));
		infoPanel.add(new JLabel(String.valueOf(month.getBeginningBalance())));
		infoPanel.add(new JLabel("All payings: "));
		infoPanel.add(new JLabel(String.valueOf(month.getSumPayouts())));
		infoPanel.add(new JLabel("All incomes: "));
		infoPanel.add(new JLabel(String.valueOf(month.getSumIncomes())));
		infoPanel.add(new JLabel(""));
		infoPanel.add(allTransactions);
		return infoPanel;
	}

	private JPanel paintPieChart(List<Category> categories, List<Transaction> transactions) {
		PieChart chart = new PieChartBuilder().width(500).height(400).theme(Styler.ChartTheme.GGPlot2).build();
		chart.getStyler().setLegendVisible(false);
		chart.getStyler().setPlotContentSize(.9);
		chart.getStyler().setStartAngleInDegrees(90);
		for (Category category : categories) {
			if (!category.isIncome()) {
				double amount = getAmountForEachCategory(category, transactions);
				if (amount != 0) {
					chart.addSeries(category.getName(), amount);
				}
			}
		}
		JPanel panel = new XChartPanel<>(chart);
		return panel;
	}

	private JTable paintTable(List<Category> categories, Month month, List<Transaction> transactions) {
		Object[][] data = new Object[categories.size()][3];
		for (int i = 0; i < categories.size(); i++) {
			data[i][0] = categories.get(i).getName();
			data[i][1] = month.getName() + " " + month.getYear();
			data[i][2] = getAmountForEachCategory(categories.get(i), transactions);
		}
		DefaultTableModel model = new DefaultTableModel(data, columnNames);
		JTable table = new JTable(model);
		return table;
	}

	private JPanel paintButtonsPanel(Month month) {
		JPanel buttons = new JPanel();
		buttons.setLayout(new GridLayout(1, 4));
		JButton payingButton = paintPayingButton(month);
		JButton incomeButtom = paintIncomeButtom(month);
		JButton newCategoryButton = paintNewCategoryButton();
		JButton reportButton = paintGetReportButton();
		buttons.add(payingButton);
		buttons.add(newCategoryButton);
		buttons.add(reportButton);
		buttons.add(incomeButtom);
		return buttons;
	}

	private JButton paintPayingButton(Month month) {
		JButton paying = new JButton("Paying");
		paying.addActionListener((e) -> {
			try {
				new GUITransaction(transactionService, categoryService, false, month.getId());
			} catch (UnsupportedLookAndFeelException | IOException ex) {
				ex.printStackTrace();
				JOptionPane.showMessageDialog(frame, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			}
		});
		return paying;
	}

	private JButton paintIncomeButtom(Month month) {
		JButton income = new JButton("Income");
		income.addActionListener((e) -> {
			try {
				new GUITransaction(transactionService, categoryService, true, month.getId());
			} catch (UnsupportedLookAndFeelException | IOException ex) {
				ex.printStackTrace();
				JOptionPane.showMessageDialog(frame, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			}
		});
		return income;
	}

	private JButton paintNewCategoryButton() {
		JButton newCategory = new JButton("New category");
		newCategory.addActionListener((e) -> {
			try {
				new GUINewCategory(categoryService, this::paintFrame);
			} catch (UnsupportedLookAndFeelException | IOException ex) {
				ex.printStackTrace();
			}
		});
		return newCategory;
	}

	private JButton paintGetReportButton() {
		JButton report = new JButton("Report");
		report.addActionListener((e) -> {
			JDialog dialog = new JDialog(frame, "Select Months", true);
			dialog.setSize(300, 150);
			dialog.setLocationRelativeTo(frame);
			String[] monthNamesArray = new String[]{};
			try {
				monthNamesArray = monthService.getAllMonthNames().toArray(new String[0]);
			} catch (IOException ex) {
				ex.printStackTrace();
			}

			Integer[] monthYearsArray = new Integer[]{};
			try {
				monthYearsArray = monthService.getAllYears().toArray(new Integer[0]);
			} catch (IOException ex) {
				ex.printStackTrace();
			}

			JComboBox<String> monthComboBox1 = new JComboBox<>(monthNamesArray);
			JComboBox<Integer> yearComboBox1 = new JComboBox<>(monthYearsArray);

			JComboBox<String> monthComboBox2 = new JComboBox<>(monthNamesArray);
			JComboBox<Integer> yearComboBox2 = new JComboBox<>(monthYearsArray);
			JButton okButton = new JButton("OK");
			okButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					String month1 = (String) monthComboBox1.getSelectedItem();
					int year1 = (int) yearComboBox1.getSelectedItem();

					String month2 = (String) monthComboBox2.getSelectedItem();
					int year2 = (int) yearComboBox2.getSelectedItem();
					dialog.dispose();

					try {
						new GUIReport(reportService, categoryService, transactionService, month1, year1, month2, year2);
					} catch (UnsupportedLookAndFeelException | IOException ex) {
						ex.printStackTrace();
						JOptionPane.showMessageDialog(frame, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
					}
				}
			});
			JPanel jPanel = new JPanel(new GridLayout(3, 2));
			jPanel.add(new JLabel("Month 1:"));
			jPanel.add(monthComboBox1);
			jPanel.add(new JLabel("Year 1:"));
			jPanel.add(yearComboBox1);
			jPanel.add(new JLabel("Month 2:"));
			jPanel.add(monthComboBox2);
			jPanel.add(new JLabel("Year 2:"));
			jPanel.add(yearComboBox2);
			jPanel.add(new JLabel());
			jPanel.add(okButton);

			dialog.getContentPane().add(jPanel);

			dialog.setVisible(true);
		});
		return report;
	}

	@Override
	public void updateTransactions(List<Transaction> transactions) {
		paintFrame();
	}
}
