package de.ase.gui.category;

import com.formdev.flatlaf.FlatLightLaf;
import de.ase.category.Category;
import de.ase.category.CategoryService;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.util.List;
import java.util.Objects;

public class GUINewCategory extends JDialog {
	private final Color[] selectedColor = {Color.WHITE};
	private JColorChooser colorChooser;
	private JDialog frame;
	JTextField nameField;

	public GUINewCategory(CategoryService categoryService, Runnable onCloseAction) throws UnsupportedLookAndFeelException, IOException {

		UIManager.setLookAndFeel(new FlatLightLaf());

		List<Category> categories = categoryService.getAllCategories();

		frame = new JDialog();
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosed(WindowEvent e) {
				onCloseAction.run();
			}
		});

		JSplitPane northSouthSplit = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
		JSplitPane southWestEastSplit = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);

		JPanel leftPanel = new JPanel(new BorderLayout());
		JPanel rightPanel = new JPanel(new GridLayout(categories.size(), 1));

		colorChooser = new JColorChooser();

		JButton colorButton = paintColorButton();

		nameField = new JTextField();
		JButton saveButton = paintSaveButton(categoryService);

		leftPanel.add(nameField, BorderLayout.CENTER);
		leftPanel.add(saveButton, BorderLayout.EAST);

		rightPanel.add(colorButton);

		southWestEastSplit.setLeftComponent(leftPanel);
		southWestEastSplit.setRightComponent(rightPanel);
		northSouthSplit.setTopComponent(southWestEastSplit);
		frame.add(northSouthSplit);
		frame.pack();
		frame.setVisible(true);
	}

	private JButton paintColorButton() {
		JButton colorButton = new JButton("Select Color");

		colorButton.addActionListener(e -> {
			int result = JOptionPane.showOptionDialog(frame, colorChooser, "Select Color",
				JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE, null, null, null);
			if (result == JOptionPane.OK_OPTION) {
				selectedColor[0] = colorChooser.getColor();
			}
		});
		return colorButton;
	}

	private JButton paintSaveButton(CategoryService categoryService) {
		JButton saveButton = new JButton("Save");
		saveButton.addActionListener(e -> {
			if (nameField.getText().isEmpty()) {
				JOptionPane.showMessageDialog(frame, "Name can not be empty", "Error", JOptionPane.ERROR_MESSAGE);
			} else if (Objects.equals(selectedColor[0], Color.WHITE)) {
				JOptionPane.showMessageDialog(frame, "Color needs to be selected", "Error", JOptionPane.ERROR_MESSAGE);
			} else {
				Category newCategory = new Category(false, nameField.getText(), selectedColor[0]);
				try {
					categoryService.saveCategory(newCategory);
				} catch (IOException ex) {
					ex.printStackTrace();
					JOptionPane.showMessageDialog(frame, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
				} catch (IllegalArgumentException ex) {
					JOptionPane.showMessageDialog(frame, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
				}
				frame.dispose();
			}
		});
		return saveButton;
	}
}