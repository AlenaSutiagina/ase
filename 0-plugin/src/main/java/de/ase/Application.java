package de.ase;

import de.ase.category.CategoryService;
import de.ase.gui.GUIAccount;
import de.ase.month.MonthService;
import de.ase.persistence.category.CategoryRepositoryBridge;
import de.ase.persistence.month.MonthRepositoryBridge;
import de.ase.persistence.transaction.TransactionRepositoryBridge;
import de.ase.report.ReportService;
import de.ase.transaction.TransactionService;

import java.awt.*;

public class Application {

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CategoryRepositoryBridge categoryRepositoryBridge = new CategoryRepositoryBridge();
					CategoryService categoryService = new CategoryService(categoryRepositoryBridge);
					MonthRepositoryBridge monthRepositoryBridge = new MonthRepositoryBridge();
					MonthService monthService = new MonthService(monthRepositoryBridge);
					TransactionRepositoryBridge transactionRepositoryBridge = new TransactionRepositoryBridge();
					TransactionService transactionService = new TransactionService(transactionRepositoryBridge, monthService);
					ReportService reportService = new ReportService(monthService);
					new GUIAccount(categoryService, monthService, transactionService, reportService);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
