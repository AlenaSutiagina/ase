package de.ase.persistence.category;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import de.ase.category.Category;
import de.ase.category.CategoryRepository;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

public class CategoryRepositoryBridge implements CategoryRepository {

	private File yamlFile;

	public CategoryRepositoryBridge() {
		yamlFile = new File("files/categories.yaml");
	}

	@Override
	public List<Category> findAll() throws IOException {
		if(!yamlFile.exists()) {
			yamlFile.createNewFile();
		}
		ObjectMapper objectMapper = new ObjectMapper(new YAMLFactory());
		TypeReference<List<Category>> typeReference = new TypeReference<List<Category>>() {
		};
		List<Category> categories = objectMapper.readValue(yamlFile, typeReference);
		return categories;
	}

	@Override
	public Optional<Category> getByName(String name) throws IOException {
		List<Category> categories = findAll();
		return categories.stream()
			.filter(category -> category.getName().equals(name))
			.findFirst();
	}

	@Override
	public void save(Category category) throws IOException {
		List<Category> categories = findAll();
		if(!yamlFile.exists()) {
			yamlFile.createNewFile();
		}
		ObjectMapper objectMapper = new ObjectMapper(new YAMLFactory());
		categories.add(category);
		objectMapper.writeValue(yamlFile, categories);
	}
}
