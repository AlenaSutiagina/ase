package de.ase.persistence.transaction;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import de.ase.transaction.Transaction;
import de.ase.transaction.TransactionRepository;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class TransactionRepositoryBridge implements TransactionRepository {

	private File yamlFile;

	public TransactionRepositoryBridge() {
		this.yamlFile = new File("files/transactions.yaml");
	}
	@Override
	public List<Transaction> getAll() throws IOException {
		if (!yamlFile.exists()) {
			yamlFile.createNewFile();
		}
		ObjectMapper objectMapper = new ObjectMapper(new YAMLFactory());
		TypeReference<List<Transaction>> typeReference = new TypeReference<List<Transaction>>() {
		};
		List<Transaction> transactions = objectMapper.readValue(yamlFile, typeReference);
		if (transactions == null) {
			transactions = new ArrayList<>();
		}
		return transactions;

	}

	@Override
	public void save(Transaction transaction) throws IOException {
		List<Transaction> transactions = getAll();
		ObjectMapper objectMapper = new ObjectMapper(new YAMLFactory());
		transactions.add(transaction);
		objectMapper.writeValue(yamlFile, transactions);
	}

	@Override
	public void delete(Transaction transaction) throws IOException {
		List<Transaction> transactions = getAll();
		ObjectMapper objectMapper = new ObjectMapper(new YAMLFactory());
		transactions.remove(transaction);
		objectMapper.writeValue(yamlFile, transactions);
	}

}