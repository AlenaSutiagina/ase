package de.ase.persistence.month;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import de.ase.month.Month;
import de.ase.month.MonthRepository;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MonthRepositoryBridge implements MonthRepository {

	private File yamlFile;

	public MonthRepositoryBridge() {
		this.yamlFile = new File("files/months.yaml");
	}

	@Override
	public List<Month> getAll() throws IOException {
		List<Month> months = new ArrayList<>();
		if (yamlFile.exists()) {
			ObjectMapper objectMapper = new ObjectMapper(new YAMLFactory());
			TypeReference<List<Month>> typeReference = new TypeReference<List<Month>>() {
			};
			months = objectMapper.readValue(yamlFile, typeReference);
			return months;
		}
		yamlFile.createNewFile();
		return months;
	}

	@Override
	public void save(Month month) throws IOException {
		if (!yamlFile.exists()) {
			yamlFile.createNewFile();
		}
		ObjectMapper objectMapper = new ObjectMapper(new YAMLFactory());
		List<Month> months = getAll();
		months.add(month);
		objectMapper.writeValue(yamlFile, months);
	}

	@Override
	public void updateBalance(Month month) throws IOException {
		if (yamlFile.exists()) {
			ObjectMapper objectMapper = new ObjectMapper(new YAMLFactory());
			List<Month> months = getAll();
			for (Month m : months) {
				if (m.getId().equals(month.getId())) {
					months.remove(m);
					break;
				}
			}
			months.add(month);
			objectMapper.writeValue(yamlFile, months);
		}
	}
}
